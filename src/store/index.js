import Vue from 'vue';
import Vuex from 'vuex';
import ownedWallets from "./modules/ownedWallets";
import providersModule from "./modules/providers";

Vue.use(Vuex);

export default new Vuex.Store({
  modules: {
    ownedWallets,
    providersModule
  }
});
