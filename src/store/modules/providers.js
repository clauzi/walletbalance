import axios from 'axios';

const state = {
    providers: [],
}

const mutations = {
    SET_PROVIDERS_LIST (state, providers) {
        state.providers = providers;
    },
}
const actions = {
    setProviders ({ commit }, providers) {
        if(state.providers.length && providers.length === state.providers.length){
            return;
        }

        return axios.get('https://api.elrondscan.com/identities',{
            params: {providers: providers.join(',')}
        }).then(response => {
            commit('SET_PROVIDERS_LIST', response.data);
        });
    },
}

const getters = {
    providers: state => state.providers
}

const providersModule = {
    state,
    mutations,
    actions,
    getters
}

export default providersModule;