const state = {
    wallets: localStorage.getItem("ownWallets") ? JSON.parse(localStorage.getItem("ownWallets")) : [],
}

const mutations = {
    UPDATE_OWN_WALLETS (state, wallets) {
        state.wallets = wallets;
    }
}
const actions = {
    getWallets({commit}){
        if(state.wallets){
            commit('UPDATE_OWN_WALLETS', state.wallets);
        }else{
            const wallets = localStorage.getItem("ownWallets") ? JSON.parse(localStorage.getItem("ownWallets")) : [];
            commit('UPDATE_OWN_WALLETS', wallets);
        }
    },
    addWalletToList ({ commit }, wallet) {
        if(!wallet.erdkey){
            return;
        }

        let wallets = localStorage.getItem("ownWallets");
        wallets = wallets ? JSON.parse(wallets) : [];
        
        wallet.id = state.wallets ? state.wallets.length + 1 : 1;

        wallets.push(wallet);

        let uniqueWallets = [...new Set(wallets)];
        localStorage.setItem("ownWallets", JSON.stringify(uniqueWallets));
        commit('UPDATE_OWN_WALLETS', uniqueWallets);
    },

    removeWalletFromList ({ commit }, wallet) {
        let Wallets = localStorage.getItem("ownWallets");
        Wallets = Wallets ? JSON.parse(Wallets) : [];
        let WalletsNewArray = [];

        if(Wallets.length > 0){
            Wallets.forEach(value => {
                if(value.erdkey !== wallet.address){
                    WalletsNewArray.push(value);
                }
            });
        }else{
            return;
        }

        let uniqueWallets = [...new Set(WalletsNewArray)];
        localStorage.setItem("ownWallets", JSON.stringify(uniqueWallets));
        commit('UPDATE_OWN_WALLETS', uniqueWallets);
    },
    
    removeWallet({ commit }) {
        localStorage.removeItem("ownWallets");
        commit('UPDATE_OWN_WALLETS', []);
    },
}

const getters = {
    wallets: state => state.wallets,
}

const ownedWallets = {
    state,
    mutations,
    actions,
    getters
}

export default ownedWallets;
