import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import VueClipboard from 'vue-clipboard2'

import { library } from '@fortawesome/fontawesome-svg-core'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'
import { fas } from '@fortawesome/free-solid-svg-icons'
import VueContentPlaceholders from 'vue-content-placeholders';
import VueNumber from 'vue-number-animation'
import VueToast from 'vue-toast-notification';
import BigNumber from "bignumber.js";

import jQuery from 'jquery'
window.$ = window.jQuery = jQuery;

import 'popper.js'
import 'bootstrap'
import './assets/app.scss'
import 'vue-toast-notification/dist/theme-sugar.css';

library.add(fas)

Vue.component('font-awesome-icon', FontAwesomeIcon)
Vue.use(VueContentPlaceholders);
Vue.use(VueClipboard)
Vue.use(VueNumber)
Vue.use(VueToast);

Vue.mixin({
  methods: {
      capitalizeFirstLetter: str => str.charAt(0).toUpperCase() + str.slice(1),
      middleStringEllipsis: (val, size = 5) => val.substring(0, size) + '...' + val.slice(-size),
      formatLocaleString: val => val ? val.toLocaleString() : null,
      getShardNameById: id =>  id === 4294967295 ? 'Metachain' : 'Shard ' + id,
      getDateFromTimestamp: (timestamp) => {
          var date = new Date(timestamp).toLocaleDateString("en-UK");
          var time = new Date(timestamp).toLocaleTimeString("en-UK");

          return date + ' ' + time;
      },
      checkIfWalletIsSmartContract: wallet => wallet.substring(0,6) === 'erd1qq',
      getPriceValue: (price, maxFractionDigits = null, divider = true, numberOfDecimals = 18) => {
          BigNumber.config({
              EXPONENTIAL_AT: 30,
          });

          let number;
          if(maxFractionDigits){
              number = BigNumber(divider ? price/Number('1'.padEnd(numberOfDecimals + 1, '0')) : price).toFixed(maxFractionDigits);
          }else{
              number = new BigNumber(divider ? price/Number('1'.padEnd(numberOfDecimals + 1 , '0')) : price);
          }

          let parts = number.toString().split(".");
          parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
          return parts.join(".");
      },
      bytesToSize(bytes) {
        var sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB'];
        if (bytes == 0) return '0 Byte';
        var i = parseInt(Math.floor(Math.log(bytes) / Math.log(1024)));
        return (bytes / Math.pow(1024, i)).toFixed(2) + ' ' + sizes[i];
     },
      
      timeDiffCalc: (dateFuture, dateNow) =>{
          let diffInMilliSeconds = Math.abs(dateFuture - dateNow) / 1000;

          // calculate years
          const years = Math.floor(diffInMilliSeconds / 31536000);
          diffInMilliSeconds -= years * 31536000;

          // calculate months
          const months = Math.floor(diffInMilliSeconds / 2628000);
          diffInMilliSeconds -= months * 2628000;

          // calculate days
          const days = Math.floor(diffInMilliSeconds / 86400);
          diffInMilliSeconds -= days * 86400;

          // calculate hours
          const hours = Math.floor(diffInMilliSeconds / 3600) % 24;
          diffInMilliSeconds -= hours * 3600;

          // calculate minutes
          const minutes = Math.floor(diffInMilliSeconds / 60) % 60;
          diffInMilliSeconds -= minutes * 60;

          let difference = '';
          if (years > 0) {
              difference += (years === 1) ? `${years} year ` : `${years} years `;
              return difference;
          }

          if (months > 0) {
              difference += (months === 1) ? `${months} month ` : `${months} months `;
              return difference;
          }

          if (days > 0) {
              difference += (days === 1) ? `${days} day ` : `${days} days `;
              return difference;
          }

          if (hours > 0) {
              difference += (hours === 0 || hours === 1) ? `${hours} hour ` : `${hours} hours `;
              return difference;
          }

          difference += (minutes === 0 || hours === 1) ? `${minutes} minutes` : `${minutes} minutes`;

          return difference;
      },
  }
})


Vue.config.productionTip = false

Vue.mixin({
  methods: {
    capitalizeFirstLetter: str => str.charAt(0).toUpperCase() + str.slice(1),
    middleStringEllipsis: (val, size = 5) => val.substring(0, size) + '...' + val.slice(-size),
    slugify: (str) => {
      str = str.replace(/^\s+|\s+$/g, '');
      str = str.toLowerCase();
      var from = "ÁÄÂÀÃÅČÇĆĎÉĚËÈÊẼĔȆÍÌÎÏŇÑÓÖÒÔÕØŘŔŠŤÚŮÜÙÛÝŸŽáäâàãåčçćďéěëèêẽĕȇíìîïňñóöòôõøðřŕšťúůüùûýÿžþÞĐđßÆa·/_,:;";
      var to   = "AAAAAACCCDEEEEEEEEIIIINNOOOOOORRSTUUUUUYYZaaaaaacccdeeeeeeeeiiiinnooooooorrstuuuuuyyzbBDdBAa------";
      for (var i=0, l=from.length ; i<l ; i++) {
        str = str.replace(new RegExp(from.charAt(i), 'g'), to.charAt(i));
      }

      str = str.replace(/[^a-z0-9 -]/g, '')
          .replace(/\s+/g, '-')
          .replace(/-+/g, '-');

      return str;
    },
    validateIPAddress(ipaddress) {
      return /^(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$/.test(ipaddress);
    }
  }
});

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
